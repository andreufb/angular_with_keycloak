import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatListModule } from '@angular/material/list'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { OAuthModule } from 'angular-oauth2-oidc';
import { ToolBarComponent } from './tools/tool-bar/tool-bar.component'
import { MatToolbarModule } from '@angular/material/toolbar';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ToolBarComponent
  ],
  imports: [
    BrowserModule,
    MatToolbarModule,
    AppRoutingModule,
    HttpClientModule,
    MatListModule,
    OAuthModule.forRoot( /* {
      resourceServer: {allowedUrls: ['http://localhost:9090/api'],
      sendAccessToken: true
    }}*/
    ), 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
