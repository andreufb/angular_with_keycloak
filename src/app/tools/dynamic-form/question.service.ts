import { Injectable } from '@angular/core';

import { DropdownQuestion } from './question-dropdown';
import { QuestionBase } from './question-base';
import { TextboxQuestion } from './question-textbox';
import { variable } from '@angular/compiler/src/output/output_ast';

@Injectable()
export class QuestionService {

  // TODO: get from a remote source of question metadata
  getData() {

    let questions: QuestionBase<any>[] = [

      new DropdownQuestion({
        key: 'endpoint',
        label: 'Type of data',
        options: [
          { key: 'physical-system', value: 'Physical system' },
          { key: 'person', value: 'Person' },
          { key: 'project', value: 'Project' },
          { key: 'mission', value: 'Mission' },
          { key: 'action', value: 'Action' },
          { key: 'location', value: 'Location' },
          { key: 'feature-of-interest', value: 'Feature of interest' },
          { key: 'observable-property', value: 'Observable property' },
          { key: 'unit-of-measurement', value: 'Unit of measurement' },
          { key: 'observation', value: 'Observation' }
        ],
        order: 1,
        hint: '',
      }),

      new TextboxQuestion({
        key: 'id',
        label: 'ID',
        order: 2
      }),
    ];

    return questions.sort((a, b) => a.order - b.order);
  };

  getId() {
    let questions: QuestionBase<any>[] = [

      new TextboxQuestion({
        key: 'id',
        label: 'ID',
        required: true,
        order: 1,
        type: "text"
      })];
    return questions.sort((a, b) => a.order - b.order);
  }

  getPhysicalSystemQuestions() {

    let questions: QuestionBase<any>[] = [

      new TextboxQuestion({
        key: 'label',
        label: 'Label',
        required: true,
        order: 1,
        type: "text"
      }),

      new TextboxQuestion({
        key: 'model',
        label: 'Model',
        required: true,
        order: 2,
        type: "text"
      }),

      new TextboxQuestion({
        key: 'manufacturer',
        label: 'Manufacturer',
        required: true,
        order: 1,
        type: "text"
      }),

      new TextboxQuestion({
        key: 'serialNumber',
        label: 'Serial number',
        required: true,
        order: 1,
        type: "text"
      }),

      new TextboxQuestion({
        key: 'uuid',
        label: 'Uuid',
        required: true,
        order: 1,
        type: "text"
      }),

      new TextboxQuestion({
        key: 'instrumentType',
        label: 'Instrument type',
        required: true,
        order: 1,
        type: "text"
      }),

      new TextboxQuestion({
        key: 'config',
        label: 'Config',
        required: true,
        order: 1,
        type: "text"
      })
    ];
    return questions.sort((a, b) => a.order - b.order);
  };

  getPersonQuestions() {
    let questions: QuestionBase<any>[] = [

      new TextboxQuestion({
        key: 'label',
        label: 'Label',
        required: true,
        order: 1,
        type: "text"
      }),

      new TextboxQuestion({
        key: 'mail',
        label: 'Mail',
        required: true,
        order: 2,
        type: "text"
      }),

      new TextboxQuestion({
        key: 'rol',
        label: 'Rol',
        required: true,
        order: 1,
        type: "text"
      })
    ];
    return questions.sort((a, b) => a.order - b.order);
  };

  getProjectQuestions() {
    let questions: QuestionBase<any>[] = [

      new TextboxQuestion({
        key: 'acronym',
        label: 'Acronym',
        required: true,
        order: 1,
        type: "text"
      }),

      new TextboxQuestion({
        key: 'title',
        label: 'Title',
        required: true,
        order: 2,
        type: "text"
      }),

      new TextboxQuestion({
        key: 'fundingEntity',
        label: 'Funding entity',
        required: true,
        order: 1,
        type: "text"
      }),

      new TextboxQuestion({
        key: 'grantAgreement',
        label: 'Grant agreement',
        required: true,
        order: 1,
        type: "number"
      }),

      new TextboxQuestion({
        key: 'initDatetime',
        label: 'Init datetime',
        required: true,
        order: 1,
        type: "date"
      }),

      new TextboxQuestion({
        key: 'endDatetime',
        label: 'End datetime',
        required: true,
        order: 1,
        type: "date"
      })
    ];
    return questions.sort((a, b) => a.order - b.order);
  };

  getMissionQuestions() {
    let questions: QuestionBase<any>[] = [

      new TextboxQuestion({
        key: 'label',
        label: 'Label',
        required: true,
        order: 1,
        type: "text"
      }),

      new TextboxQuestion({
        key: 'description',
        label: 'Description',
        required: true,
        order: 2,
        type: "text"
      })
    ];

    return questions.sort((a, b) => a.order - b.order);
  };

  getActionQuestions() {
    let questions: QuestionBase<any>[] = [

      new TextboxQuestion({
        key: 'description',
        label: 'Description',
        required: true,
        order: 1,
        type: "text"
      }),

      new TextboxQuestion({
        key: 'uuid',
        label: 'Uuid',
        required: true,
        order: 2,
        type: "text"
      }),

      new TextboxQuestion({
        key: 'label',
        label: 'Label',
        required: true,
        order: 1,
        type: "text"
      }),

      new TextboxQuestion({
        key: 'time',
        label: 'Time',
        required: true,
        order: 1,
        type: "date"
      }),

      new TextboxQuestion({
        key: 'actionTime',
        label: 'Action time',
        required: true,
        order: 1,
        type: "date"
      }),

      new TextboxQuestion({
        key: 'physicalSystem_idphisicalSystem',
        label: 'Physical System ID',
        required: true,
        order: 1,
        type: "number"
      }),

      new TextboxQuestion({
        key: 'project_idproject',
        label: 'Project ID',
        required: true,
        order: 1,
        type: "number"
      })
    ];
    return questions.sort((a, b) => a.order - b.order);
  };

  getLocationQuestions() {
    let questions: QuestionBase<any>[] = [

      new TextboxQuestion({
        key: 'latitude',
        label: 'Latitude',
        required: true,
        order: 1,
        type: "float"
      }),

      new TextboxQuestion({
        key: 'longitude',
        label: 'Longitude',
        required: true,
        order: 2,
        type: "float"
      }),

      new TextboxQuestion({
        key: 'altitude',
        label: 'Altitude',
        required: true,
        order: 1,
        type: "float"
      }),

      new TextboxQuestion({
        key: 'description',
        label: 'Description',
        required: true,
        order: 1,
        type: "text"
      }),
    ];
    return questions.sort((a, b) => a.order - b.order);
  };

  getFeatureOfInterestQuestions() {
    let questions: QuestionBase<any>[] = [

      new TextboxQuestion({
        key: 'label',
        label: 'Label',
        required: true,
        order: 1,
        type: "text"
      }),

      new TextboxQuestion({
        key: 'link',
        label: 'Link',
        required: true,
        order: 2,
        type: "url"
      }),

      new TextboxQuestion({
        key: 'description',
        label: 'Description',
        required: true,
        order: 1,
        type: "text"
      })
    ];
    return questions.sort((a, b) => a.order - b.order);
  };

  getObservablePropertyQuestions() {
    let questions: QuestionBase<any>[] = [

      new TextboxQuestion({
        key: 'label',
        label: 'Label',
        required: true,
        order: 1,
        type: "text"
      }),

      new TextboxQuestion({
        key: 'link',
        label: 'Link',
        required: true,
        order: 2,
        type: "url"
      }),

      new TextboxQuestion({
        key: 'description',
        label: 'Description',
        required: true,
        order: 1,
        type: "text"
      })
    ];
    return questions.sort((a, b) => a.order - b.order);
  };

  getUnitOfMeasurementQuestions() {
    let questions: QuestionBase<any>[] = [

      new TextboxQuestion({
        key: 'code',
        label: 'Code',
        required: true,
        order: 1,
        type: "text"
      }),

      new TextboxQuestion({
        key: 'link',
        label: 'Link',
        required: true,
        order: 2,
        type: "url"
      }),

      new TextboxQuestion({
        key: 'description',
        label: 'Description',
        required: true,
        order: 1,
        type: "text"
      })
    ];
    return questions.sort((a, b) => a.order - b.order);
  };

  getObservationQuestions() {
    let questions: QuestionBase<any>[] = [

      new TextboxQuestion({
        key: 'value',
        label: 'Value',
        required: true,
        order: 1,
        type: "float"
      }),

      new TextboxQuestion({
        key: 'quality',
        label: 'Quality',
        required: true,
        order: 2,
        type: "number"
      }),

      new TextboxQuestion({
        key: 'description',
        label: 'Description',
        required: true,
        order: 1,
        type: "text"
      })
    ];
    return questions.sort((a, b) => a.order - b.order);
  };




}
