import { Component, Input, OnInit, Output, EventEmitter }  from '@angular/core';
import { FormGroup }                 from '@angular/forms';

import { QuestionBase }              from './question-base';
import { QuestionControlService }    from './question-control.service';

import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.css'],
  providers: [ QuestionControlService ]
})
export class DynamicFormComponent implements OnInit {

  @Input() questions: QuestionBase<any>[] = [];
  @Input() isDialog: boolean;

  @Output() close = new EventEmitter<any>();
  @Output() submit = new EventEmitter<any>();

  form: FormGroup;
  payLoad = '';
  endpoint = '';
  method = '';


  constructor(private qcs: QuestionControlService, private _snackBar: MatSnackBar) { }

  ngOnInit() {
    this.form = this.qcs.toFormGroup(this.questions);
  }

  onSubmit() {
    this._snackBar.open(`Submited Successfully`, '', {
      duration: 4000,
      panelClass: 'snackbar-success',
      verticalPosition: 'top',
      horizontalPosition: 'center'
    }).afterOpened().subscribe(() => {
      this.submit.emit(this.form.value);
      this.payLoad = JSON.stringify(this.form.value);
    });
  }

  onCancel() {
    this.close.emit(null);
  }
}
