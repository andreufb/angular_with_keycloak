import { ButtonSettings } from './button-settings';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']

})
export class ButtonComponent implements OnInit {

  @Input() buttonSettings: ButtonSettings;
  @Input() parent;

  constructor() { }

  ngOnInit(): void {
  }

  clickedButton;

}
