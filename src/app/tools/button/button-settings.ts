export class ButtonSettings {
  disabled: boolean;
  label: string;
  type: string;
  route?: string;
  constructor(options: {
      disabled?: boolean,
      label?: string,
      type?: string,
      route?: string,
    } = {}) {
    this.disabled = options.disabled || false;
    this.label = options.label || '';
    this.type = options.type || '';
    this.route = options.route;
  }
}
