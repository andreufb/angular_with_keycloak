import { Component } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { JwksValidationHandler } from 'angular-oauth2-oidc-jwks';
import { keycloakConfig } from '../assets/sso.config';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import * as jwt_decode from "jwt-decode";
import { isUndefined } from 'util';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'SSO portected web with diferent IdPs';

  avatar

  information = {
    claims: null,
    access_time: null,
    id_token: null,
    access_token: null,
    refresh_token: null,
    valid_token: null,
    scopes: null
  }


  constructor(private oauthService: OAuthService, private http: HttpClient) {
    this.configureSingleSignOn()
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.oauthService.loadDiscoveryDocumentAndTryLogin();
  };

  configureSingleSignOn() {
    this.oauthService.configure(keycloakConfig);
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.oauthService.stopAutomaticRefresh();
    this.oauthService.loadDiscoveryDocumentAndTryLogin();
  }


  login() {
    this.oauthService.initImplicitFlow();
  }

  logout() {
    this.oauthService.revokeTokenAndLogout();
    this.oauthService.resetImplicitFlow();
    this.oauthService.logOut();
  }
  refresh_token() {
    this.oauthService.silentRefresh();
    this.oauthService.refreshToken();
  }

  account() {
    window.open("https://aai-test.emso.eu:8443/auth/realms/EMSO/account/", "_blank")
  }

  get token() {
    this.information.claims = this.oauthService.getIdentityClaims();
    this.information.access_time = this.oauthService.getAccessTokenExpiration();
    this.information.id_token = this.oauthService.getIdToken();
    this.information.access_token = this.oauthService.getAccessToken();
    this.information.valid_token = this.oauthService.hasValidAccessToken();
    this.information.scopes = this.oauthService.getGrantedScopes();
    this.information.refresh_token = this.oauthService.getRefreshToken();
    try{
      this.avatar = jwt_decode(this.information.access_token).avatar_url;
      if (this.avatar === undefined || this.avatar=='') {
        this.avatar = '../assets/generic_user.png'
      }
    } catch(error) {
      this.avatar = '../assets/generic_user.png'
    }
    /*  */
    /* window.alert(this.avatar) */

    return this.information.claims ? this.information.claims : null;
  };


}

