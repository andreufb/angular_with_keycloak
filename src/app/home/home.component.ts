import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { formatDate } from '@angular/common';
import { AppComponent } from '../app.component';
import * as jwt_decode from "jwt-decode";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  /*Used for the EGI Check-in*/
  user_info = jwt_decode(this.appvals.information.access_token);
  information = this.appvals.information;
  roles

  ngOnInit() {
    this.roles = this.user_info.realm_access.roles
    console.log(this.user_info)
  };

  constructor(public appvals: AppComponent, private http: HttpClient) { };

  information = this.appvals.information

  access_time = formatDate(this.information.access_time, 'hh:mm:ss', 'en-US')
  date = formatDate(this.information.claims.auth_time * 1000, 'dd/MM/yyyy hh:mm:ss', 'en-US')

  objectKeys = Object.keys;

}
