import { AuthConfig } from 'angular-oauth2-oidc';

export const keycloakConfig: AuthConfig = {
    issuer: 'https://aai-test.emso.eu:8443/auth/realms/EMSO',
    dummyClientSecret: '8bc2549e-6196-4839-a33f-c1eb77af7f5b',
    redirectUri: window.location.origin + '/home',
    clientId: 'proba_1',
    responseType: 'code',
    scope: 'openid profile email offline_access emso_scopes',
    showDebugInformation: true
};
