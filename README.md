# SSO Angular Aplication with multiple Identity Providers

Project generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.8.

With the SSO once you are logged into the [EGI Check-in AAI](https://aai.egi.eu/registry) or one of the others identity providers presented in the web, you can access the resources that this application has depending on th epermissions granted to your account. If previously you have identified yourself using one of the presented identity providers platforms you don't need to log in in this application to access its resources.

## Steps:

#### 1. Generate a new angular project and install angular CLI
```
ng new project_name
npm install
npm install -g @angular/cli
```

#### 2. Generate a component to protect it

`ng g c component_name`

#### 3. Go to the `app.module.ts` file and import and declare the following modules:
```
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { OAuthModule } from 'angular-oauth2-oidc';
import { HomeComponent } from './home/home.component';
import { MatListModule } from '@angular/material/list';
```

#### 4. Generate a TypeScript file to configure the authentication process:
The issuer is the Identity Provider, in this example the EGI Check-in development instance. The `AuthConfig` module is also needed.

You always need the issuer, client ID and type of response. Additionally, if you have a client secret you need to pass it, you can configure more options like redirect URL after loggin, the desired scopes and the endpoint where the user info is located.

```
import { AuthConfig } from 'angular-oauth2-oidc';

export const EGI_authConfig: AuthConfig = {

    issuer: 'https://aai-dev.egi.eu/oidc/',
    redirectUri: window.location.origin + '/home',
    clientId: 'b21aab5f-8ca2-4186-aaf0-c8abaa11bbca',
    userinfoEndpoint: 'https://aai-dev.egi.eu/oidc/userinfo',
    dummyClientSecret: 'E1gy7lUB_l9ED0qYJqn-kVXllTKsGVb16ZtSQkGkBm3kJ52gJ-6CJwed1h3odw1EcFKPesegFyNra8vQ292DUg',
    responseType: 'code',
    scope: 'openid profile email offline_access',
    showDebugInformation: true
};
```

Here it is were you also need to configure and export all the issuers (identity providers) that you want for your application.

#### 5. Open the `app-routing.module.ts` and protect the resources/components:
Define the components to protect:
```
const routes: Routes = [
  {path: 'home', component: HomeComponent}
]
```
Import the `RouterModule.forRoot()` and indicate the list of routes/resources to protect. This resources will only be avialable for the user if its authenticated:
```
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
```
It is also recomended to add a redirection route pointing the principal component for leting the user select or re-select the IdP in case of some authentication or authorization problem.

#### 6. Put the router outlet in the `app.component.html` and let it work if there is a token:
`<router-outlet *ngIf="token"></router-outlet>`

In this HTML document is also were you need to let the user decide what IdP want to use if there is no token. Example:
```
<div *ngIf="!token">
    <table>
      <tr>
        <td>
          <a (click)="login('EGI')">EGI IdP</a>
        </td>
        <td>
          <a (click)="login('EMSO')">Our own IdP</a>
        </td>
      </tr>
    </table>
  </div>
```



#### 7. Open the `app.component.ts` and configure as following:
Import the following modules and IdP configuration constants (all the IdP configured at `sso.config.ts`):
```
import { Component } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { JwksValidationHandler } from 'angular-oauth2-oidc-jwks';
import { EGI_authConfig, EMSO_authConfig } from './sso.config';
```

Try to get the provider from the local storage and define the important information you want from the identity service:

```
export class AppComponent {
  title = 'SSO portected web with diferent IdPs';
  provider = localStorage.getItem('provider')

  information = {
    provider: this.provider,
    claims: null,
    access_time: null,
    id_token: null,
    access_token: null,
    valid_token: null,
    scopes: null
  }
```
Now declare the `0AuthService` service inside the constructor, make an if/else conditional statement for cheking if the provider is set in the local storage and execute the pertinent functon for creating the authorization method based in this provider value:
```
  constructor(private oauthService: OAuthService) {
    if (localStorage.getItem('provider') == 'EGI') {
      this.EGI();
    }
    else {
      if (localStorage.getItem('provider') == 'EMSO') {
        this.EMSO();
      }
    }
  };
```
This way the AAI will only start if the user selected an IdP. In this case the called functions are the following:
```
EGI() {
    this.oauthService.configure(EGI_authConfig);
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.oauthService.stopAutomaticRefresh();
    this.oauthService.loadDiscoveryDocumentAndLogin();
  };

  EMSO() {
    this.oauthService.configure(EMSO_authConfig);
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.oauthService.stopAutomaticRefresh();
    this.oauthService.loadDiscoveryDocumentAndLogin();
  };
```
They use the OAuthService for configuring the IdP using the configuration exported from `sso.config.ts`. We also stop the automatic token refresh for leting the user control it and start the login method once the user has selected the IdP.


#### 8. Define the following functions:

##### 8.1 Login:
Save the user selected IdP and reload the component for using the selected IdP to get the user credentials:
```
login(provider) {
    localStorage.setItem('provider', provider);
    location.reload();
  };
```
##### 8.2 Logout:
Logout from the SSO without revoking the token in case the user want to use the web again without login in again:
```
login(provider) {
    localStorage.setItem('provider', provider);
    location.reload();
  };
```
##### 8.3 Logout + token revokation:
Logout from the SSO revoking the token and stoping the implicit flow.
```
full_logout() {
    this.oauthService.revokeTokenAndLogout();
    this.oauthService.resetImplicitFlow();
    this.oauthService.logOut();
    localStorage.removeItem('provider')
    window.location.assign('http://' + window.location.hostname + ':4200/')
  }
```
##### 8.4 Refresh token:
```
refresh_token() {
    this.oauthService.silentRefresh();
    this.oauthService.refreshToken();
  };
```
##### 8.5 Delete the selected IdP:
With this functionality the user can chose another IdP in case of need:
```
delete_provider() {
    localStorage.removeItem('provider');
    location.reload();
  };
```
#### 9. Get the token and user credentials:
Finally, define the functionality to get the token and claims from the Identity Provider:
```
get token() {
    this.information.claims = this.oauthService.getIdentityClaims();
    this.information.access_time = this.oauthService.getAccessTokenExpiration();
    this.information.id_token = this.oauthService.getIdToken();
    this.information.access_token = this.oauthService.getAccessToken();
    this.information.valid_token = this.oauthService.hasValidAccessToken();
    this.information.scopes = this.oauthService.getGrantedScopes();

    return this.information.claims ? this.information.claims : null;
  };

```
In this case we save the user info into the JSON `information` object

#### 9. (In case of EGI Check-in) Get the user additonal information:

Define the header to use the access token getted at the previous step:
```
import { HttpClient, HttpHeaders } from '@angular/common/http';

httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }).set('Authorization', `Bearer ${this.access_token}`)
  };
```
Define the http service and use it to get the user information:
```
constructor(private http: HttpClient) { }

ngOnInit(): void {
    this.http.get('https://aai-dev.egi.eu/oidc/userinfo', this.httpOptions).pipe(retry(1)).subscribe(
      data => {
        this.user_info = JSON.parse(JSON.stringify(data));
    });
  }
```


## Run the project

`ng serve` or `ng serve --host desired_IP`

In case of not indicating the IP navigate to `http://localhost:4200/` and, in the case of indicating the IP, nvaigate to `http://desired_IP:4200/`.

The app will automatically reload if you change any of the source files.


## References:

Angular: https://angular.io/guide/file-structure

OAuthService: https://manfredsteyer.github.io/angular-oauth2-oidc/docs/injectables/OAuthService.html


npm install jwt-decode


## Deployment with the PathLocationStrategy

Once you installed the apache server, go to /etc/httpd/conf and modify the httpd.conf with the `AllowOverride` directive set to `All`:

```
<Directory "/var/www/<folder_name>">
    Options Indexes FollowSymLinks
    AllowOverride All
    Require all granted
</Directory>
```
This way we can use the configuration inside an .htaccess file. 
So now, we need to go to the folder with the project `/var/www/<folder_name>` and generate the .htaccessfile: `vim .htaccess`.
Inside this file we need to add the following snippet:
```
RewriteEngine on
RewriteCond %{REQUEST_FILENAME} -s [OR]
RewriteCond %{REQUEST_FILENAME} -l [OR]
RewriteCond %{REQUEST_FILENAME} -d
RewriteRule ^.*$ - [NC,L]
RewriteRule ^(.*) /index.html [NC,L]
```
Now, when the user asks for a resource, the request will be reqrited to gice the user the `index.html` file allowing us to reload the page and ask for its resources.

The base href in the `index.html` file also needs to be changed so the browser finds the Javascript and CSS files: `<base href=".">`.
